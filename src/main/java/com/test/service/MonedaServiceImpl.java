/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.service;

import com.test.dao.MonedaDAO;
import com.test.entity.Moneda;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author leonardo
 */
@Service
public class MonedaServiceImpl implements MonedaService{

    @Autowired
    private MonedaDAO monedaDAO;

    @Override
    public List<Moneda> findAll() {
        
        List<Moneda> listMoneda = monedaDAO.findAll();
        return listMoneda;
    }

    @Override
    public Moneda findById(int id) {
        Moneda moneda = monedaDAO.findById(id);
        return moneda;
    }

    @Override
    public void save(Moneda moneda) {
        monedaDAO.save(moneda);
    }

    @Override
    public void deleteById(int id) {
        monedaDAO.deleteById(id);
    }
    
}
