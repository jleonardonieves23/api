/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.controller;

import com.test.entity.Moneda;
import com.test.service.MonedaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cripto")
public class MonedaRestController {

    @Autowired
    private MonedaService monedaService;

    @GetMapping("/monedas")
    public List<Moneda> findAll() {
        return monedaService.findAll();
    }

    @GetMapping("/moenda/{monedaId}")
    public Moneda getUser(@PathVariable int userId) {
        Moneda moneda = monedaService.findById(userId);

        if (moneda == null) {
            throw new RuntimeException("Moneda id not found -" + userId);
        }

        return moneda;
    }

    @PostMapping("/moneda")
    public Moneda addMoneda(@RequestBody Moneda moneda) {
        moneda.setId(0);

        monedaService.save(moneda);

        return moneda;

    }

    @PutMapping("/moneda")
    public Moneda updateMoneda(@RequestBody Moneda moneda) {

        monedaService.save(moneda);

        return moneda;
    }

    @DeleteMapping("moneda/{monedaId}")
    public String deteteUser(@PathVariable int monedaId) {

        Moneda moneda = monedaService.findById(monedaId);

        if (moneda == null) {
            throw new RuntimeException("User id not found -" + monedaId);
        }

        monedaService.deleteById(monedaId);

        return "Deleted user id - " + monedaId;
    }
}
