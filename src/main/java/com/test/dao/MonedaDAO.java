/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.dao;

import com.test.entity.Moneda;
import java.util.List;

/**
 *
 * @author leonardo
 */
public interface MonedaDAO {
    public List<Moneda> findAll();

    public Moneda findById(int id);

    public void save(Moneda moneda);

    public void deleteById(int id);
}
