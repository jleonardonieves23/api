/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.dao;

import com.test.entity.Moneda;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author leonardo
 */
public class MonedaDAOImpl implements MonedaDAO{
    
    @Autowired
	private EntityManager entityManager;

    @Override
    public List<Moneda> findAll() {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Moneda> theQuery = currentSession.createQuery("from Moneda", Moneda.class);

        List<Moneda> moneda = theQuery.getResultList();

        return moneda;
    }

    @Override
    public Moneda findById(int id) {
        
        Session currentSession = entityManager.unwrap(Session.class);

        Moneda moneda = currentSession.get(Moneda.class, id);

        return moneda;
    }

    @Override
    public void save(Moneda moneda) {
        Session currentSession = entityManager.unwrap(Session.class);

        currentSession.saveOrUpdate(moneda);
    }

    @Override
    public void deleteById(int id) {
        
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Moneda> theQuery = currentSession.createQuery("delete from Moneda where id=:monedaId");

        theQuery.setParameter("monedaId", id);
        theQuery.executeUpdate();
    }
    
}
